![IISightSDK Header](https://11sight.com/11sightwp/uploads/2017/06/11sight-logo.png)

# IISightSDK

## Overview

[11Sight](https://11sight.com) iOS Framework.

## Features

1. Video call
2. Audio call
3. Chat

Supported localizations: English and Turkish.

## Requirements

iOS 9

## Setup

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate IISightSDK into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
platform :ios, '9.0'

target '<Your Target Name>' do
    pod 'IISightSDK'
end
```

Then, run the following command:

```bash
$ pod install
```
or 
```bash
$ pod update
```

### Manually

If you prefer not to use any dependency managers, you can integrate `IISightSDK` into your project manually.

#### Embedded Framework

- Download [IISightSDK](https://11sight.com/sdk/downloads/ios/11sight-ios-sdk-4.2.0.zip) and unzip it to your preferred folder. 
- Next, select your application project in the Project Navigator (blue project icon) to navigate to the target configuration window and select the application target under the "Targets" heading in the sidebar.
- In the tab bar at the top of that window, open the "General" panel.
- Drop `IISighSDK.framework` under "Embedded Binaries" section.

> It should appear under "Linked Frameworks and Binaries" as well.

- And that's it!

### Additional Configurations

#### Capabilities
- Select your application project in the Project Navigator to navigate to the target configuration window and select the application target under the "Targets" heading in the sidebar.
- In the tab bar at the top of that window, open the "Capabilities" panel.
- Enable "Push Notifications"
- Enable "Background Modes", select "Audio, AirPlay, and Picture in Picture" and "Voice over IP"
> If you are using XCode 9, "Voice over IP" is removed from Capabilities and you have to add it manually to info.plist.
Your "info.plist" should look like this
```ruby
<key>UIBackgroundModes</key>
<array>
<string>audio</string>
<string>voip</string>
</array>
```


#### Info.plist
Add the following keys:
- NSAppleMusicUsageDescription
- NSCameraUsageDescription
- NSMicrophoneUsageDescription
- NSPhotoLibraryUsageDescription

#### Required configuration for Objective-C projects

- Select your application project in the Project Navigator (blue project icon) to navigate to the target configuration window and select the application target under the "Targets" heading in the sidebar.
- In the tab bar at the top of that window, open the "Build Settings" panel.
- Set `Always Embed Swift Standard Libraries` to `YES`

## Usage
### AppDelegate
```ruby
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@import IISightSDK;

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
        if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
        {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
                if( !error ){
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                }
            }];
        }

        [[IISightSDKManager sharedManager] startWithPartnerUrl:@"YOUR_PARTNER_URL"];
        return YES;
    }

    #pragma mark - iOS 8.0 Notifications
    - (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
        BOOL belongsToSDK = [[IISightSDKManager sharedManager] localNotificationBelongsToIISight:notification.category];

    }

    - (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
        BOOL belongsToSDK = [[IISightSDKManager sharedManager] localNotificationBelongsToIISight:notification.category];
        completionHandler();
    }

    #pragma mark - iOS 10.0 Notifications
    -(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
        BOOL belongsToSDK = [[IISightSDKManager sharedManager] localNotificationBelongsToIISight:response.actionIdentifier];
    }

    -(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
        BOOL belongsToSDK = [[IISightSDKManager sharedManager] localNotificationBelongsToIISight:notification.request.content.categoryIdentifier];
        completionHandler(UNNotificationPresentationOptionAlert);
    }
```

### Sign in
```ruby
    - (void) login {
        [[IISightSDKManager sharedManager] setLoginDelegate:self];
        [[IISightSDKManager sharedManager] login_userWithEmail:@"USERNAME" password:@"PASSWORD"];
    }                               
    #pragma mark - Login Delegate
    -(void)loginSuccessful{
        // Successful login
    }

    -(void)loginFailedWithErrorMessage:(NSString *)errorMessage {
        // Login failed
    }                               
```

### Making call
```ruby
    - (void) makeCall {
        /*
        *   Available callTypes: [.Video, .Audio, .Chat]
        */
        [[IISightSDKManager sharedManager] startOutgoingCall:@"BUTTON_ID" trackerId:nil callType:IISightCallType.Video];
    }
```
### Sign out
```ruby
    - (void) logout {
        [[IISightSDKManager sharedManager] setLogoutDelegate:self];
        [[IISightSDKManager sharedManager] logout_user];
    }
    #pragma mark - Logout Delegate
    - (void) logoutSuccessful{
        // Successful sign out
    }

    - (void) logoutFailedWithErrorMessage:(NSString *)errorMessage{
        // Sign out failed
    }
```

### Properties
Authorization status
```ruby
    @property (nonatomic) BOOL isLoggedIn;
```
Current connection status
```ruby
    @property (nonatomic, assign) IISightConnectionStatus currentConnectionStatus;
```
Ringtone for incoming call
```ruby
    @property (strong, nonatomic) NSString * ringtone;
```
> Ringtone file name with extension only. (Ex. "ringtone.wav")

## Resources

* [Documentation](https://11sight.com/)
* [Blog](https://11sight.com/blog/)
* [Website](https://11sight.com)
* [Contact us](https://11sight.com/contact/)

## Author

11Sight Inc.

## License

11Sight Inc.: Copyright 2017-2022 11Sight Inc. All Rights Reserved. Use of this software is subject to the terms and conditions of the 11Sight [terms and conditions](https://11sight.com/wp-content/uploads/2016/08/11Sight-Terms-of-Service.pdf).

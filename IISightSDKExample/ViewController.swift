//
//  ViewController.swift
//  IISightSDKExample
//
//  Created by Berdikhan on 12/12/2018.
//  Copyright © 2018 11Sight. All rights reserved.
//

import UIKit
import IISightSDK

class ViewController: UIViewController, IISightSDKLoginDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func login(_ sender: Any) {
        IISightSDKManager.shared().loginDelegate = self
        IISightSDKManager.shared().login_user(withEmail: "", password: "")
    }
    
    func loginSuccessful() {
        print("success")
    }
    
    func loginFailedWithErrorMessage(_ errorMessage: String?) {
        print(errorMessage)
    }
    
    
}

